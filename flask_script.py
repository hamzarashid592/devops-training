#!/usr/bin/python3

from flask import Flask

app=Flask(__name__)

@app.route("/")
def homepage():
    return "My first flask script..!!"

if __name__=="__main__":
    app.run()

